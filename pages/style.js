import styled from 'styled-components'

export const Container = styled.section`

  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;
  padding: 32px;


`

export const BotaoIniciar = styled.h3`

font-weight: normal;
flex: 1;
margin: 0;
width: 100%;
height: 100%;
display: flex;
justify-content: center;
align-items: center;
cursor: pointer;


:hover{
    background-color: yellow;
    color: black;
    transition: 1.9s;
    border-radius: 23px;
}

`


export const DuplaCard = styled.section`

  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  height: 100%;

`