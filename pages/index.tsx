import { Container, BotaoIniciar, DuplaCard } from './style'
import Card from '../components/Card'
import Link from 'next/link'
import { useState } from 'react'
import EntradaNumerica from '../components/EntradaNumerica'

const Form = () => {

  const [ qtdPortas, setQtdPortas ] = useState(3)
  const [ portaPremiada, setPortaPremiada ] = useState(1)


  return (

    <Container>
      <DuplaCard>
        <Card  bgcolor="blue"> Monty Hall </Card>
        <Card> <EntradaNumerica text='Qtd Portas ? ' value={qtdPortas} onChange={e => setQtdPortas(e)} /> </Card>
      </DuplaCard>
      <DuplaCard>
        <Card> <EntradaNumerica text='Porta premiada ? ' value={portaPremiada} onChange={e => setPortaPremiada(e)} /> </Card>
        
        <Card bgcolor="#827717 "> 
          <Link href={`/jogo/${qtdPortas}/${portaPremiada}`} >
            <BotaoIniciar> iniciar </BotaoIniciar>
          </Link>
        </Card>
      </DuplaCard>
      
    </Container>

  )
}
export default Form