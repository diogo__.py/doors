import { Container } from './style'

interface CartaoProps {
    bgcolor?: string
    children?: any
}

const Card = (props: CartaoProps) => {

    return (
        <Container style={{backgroundColor: props.bgcolor ?? '#ccc'}}>

            {props.children}

        </Container>
    )
}

export default Card