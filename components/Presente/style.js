import styled from 'styled-components'

export const Container = styled.section`

display: flex;
position: relative;
flex-direction: column;
justify-content: center;
align-items: center;

`
export const Tampa = styled.div`

width: 100px;
height: 25px;
background-color: #7aa944;

`
export const Corpo = styled.div`

width: 90px;
height: 60px;
background-color: #5c7e;

`
export const Laco1 = styled.div`

background-color: red;
width: 15px;
height: 85px;
position: absolute;

`

export const Laco2 = styled.div`

background-color: red;
width: 90px;
height: 15px;
position: absolute;
top: 40px;

`