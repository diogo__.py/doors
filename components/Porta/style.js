import styled from 'styled-components'

export const Container = styled.section`

position: relative;
display: flex;
width: var(--area-porta-largura);
height: var(--area-porta-altura);
flex-direction: column;
align-items: center;
margin: 5px;
cursor: pointer;
padding: 10px;

:hover{
    background-color: green;
    transition: 1.7s;
}

`

export const PortaStyle = styled.div`

display: flex;
justify-content: space-around;

align-items: center;
background-color: chocolate;
flex-grow: 1;


`

export const Numero = styled.div`

font-size: 3rem;
position: relative;
left: -28px;

`

export const Macaneta = styled.div`

width: 20px;
height: 20px;
border-radius: 50%;
background-color: brown;
border: 2px solid red;

:hover{
    background-color: yellow;
    transition: 1.7s;
}


`


export const Chao = styled.div`

height: 10px;
width: 100%;
background-color: #ddd;

`
