export default class PortaModel {

    #numero: number
    #temPresente: boolean
    #selecinada: boolean
    #aberta: boolean

    constructor(numero: number, temPresente = false, selecinada = false, aberta = false){

        this.#numero = numero
        this.#temPresente = temPresente
        this.#selecinada = selecinada
        this.#aberta = aberta

     }

     get numero(){
        return this.#numero
     }

     get temPresente(){
        return this.#temPresente
     }

     get selecinada(){
        return this.#selecinada
     }

     get aberta(){
        return this.#aberta
     }

     get fechada(){
      return !this.aberta
   }

     desselecionar(){
        const selecinada = false
        return new PortaModel(this.numero, this.temPresente, selecinada, this.aberta)
     }

     alternarSelecao(){
         const selecinada = !this.selecinada
         return new PortaModel(this.numero, this.temPresente, selecinada, this.aberta)
     }

     abrir(){
        const aberta = true
        return new PortaModel(this.numero, this.temPresente, this.selecinada, aberta)
    }
}